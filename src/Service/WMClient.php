<?php

namespace App\Service;

use App\Exception\WMClientException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class WMClient
 * @package App\Service
 */
class WMClient
{
    const HOST = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com';

    protected $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function register(array $customer): string
    {
        $response = $this->client->request(
            Request::METHOD_POST,
            self::HOST.'/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'json' => $customer
            ]);

        if($response->getStatusCode() == Response::HTTP_OK) {
            $data = $response->toArray();
            return $data['paymentDataId'];
        }

        throw new WMClientException($response->getContent(), $response->getStatusCode());
    }

}

