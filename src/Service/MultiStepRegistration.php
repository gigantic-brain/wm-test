<?php

namespace App\Service;

use App\Entity\Customer;
use App\Form\MultiStep\MultiStepForm;
use App\Form\Type\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MultiStepRegistration
 * @package App\Service
 */
class MultiStepRegistration extends MultiStepForm
{

    protected $client;
    protected $entityManager;

    public function __construct(
        WMClient $client,
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        RouterInterface $router,
        SessionInterface $session
    )
    {
        $this->client = $client;
        $this->entityManager = $entityManager;

        parent::__construct($formFactory, $router, $session);
    }

    protected function configureSteps(): void
    {
        $this->steps = [
            1 => [
                'title' => 'Insert personal information',
                'next' => [
                    'label' => 'Next',
                    'routeName' => 'app_registration_step'
                ]
            ],
            2 => [
                'title' => 'Insert address information',
                'next' => [
                    'label' => 'Next',
                    'routeName' => 'app_registration_step'
                ],
                'previous' => [
                    'label' => 'Previous',
                    'routeName' => 'app_registration_step'
                ]
            ],
            3 => [
                'title' => 'Insert payment information',
                'previous' => [
                    'label' => 'Previous',
                    'routeName' => 'app_registration_step'
                ],
                'next' => [
                    'label' => 'Next',
                ]
            ]
        ];
    }

    protected function formClass(): string
    {
        return RegistrationType::class;
    }


    protected function entityClass(): string
    {
        return Customer::class;
    }

    protected function stateKey(): string
    {
        return 'multi_step_registration';
    }

    public function beforeFinish()
    {
        $this->entityManager->persist($this->entity);
        $this->entityManager->flush();

        $paymentDataId = $this->client->register([
            'customerId' => $this->entity->getId(),
            'owner' => $this->entity->getAccountOwner(),
            'iban' => $this->entity->getIban()
        ]);

        $this->entity->setPaymentDataId($paymentDataId);
        $this->entityManager->flush();
        $this->entityManager->clear($this->entityClass());

        $this->session->set('user_payment_data_id', $paymentDataId);
    }

}