<?php

namespace App\Controller;

use App\Service\MultiStepRegistration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function indexAction(MultiStepRegistration $registration)
    {
        return $this->render('index/index.html.twig', [
            'registration' => $registration
        ]);
    }

    /**
     * @Route("/start-again", name="app_start_again")
     */
    public function startAgainAction(SessionInterface $session)
    {
        $session->clear();

        return $this->redirectToRoute('app_registration_step');
    }

}