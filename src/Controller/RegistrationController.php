<?php

namespace App\Controller;

use App\Exception\WMClientException;
use App\Service\MultiStepRegistration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

/**
 * Class RegistrationController
 * @package App\Controller
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration/success", name="app_registration_success")
     */
    public function successAction(MultiStepRegistration $registration)
    {
        if(!$registration->isFinished()) {
            throw $this->createNotFoundException();
        }

        return $this->render('registration/success.html.twig', [
            'registration' => $registration
        ]);
    }

    /**
     * @Route("/registration/{stepNumber}", name="app_registration_step")
     */
    public function stepAction(Request $request, MultiStepRegistration $registration, LoggerInterface $logger, int $stepNumber = 1): Response
    {
        if(!$registration->isStepAllowed($stepNumber) || $registration->isFinished()) {
            throw $this->createNotFoundException();
        }

        try {
            if($registration->isInterrupted()) {
                $interruptedStepNumber = $registration->getInterruptedStepNumber();
                $registration->removeInterrupted();
                return $this->redirectToRoute('app_registration_step', [
                    'stepNumber' => $interruptedStepNumber
                ]);
            }

            $registration->setStepNumber($stepNumber);

            $form = $registration->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $registration->saveState();

                if($registration->isLastStep()) {

                    $registration->finish();

                    return $this->redirectToRoute('app_registration_success');
                }

                return $this->redirect($registration->getNextStepUrl());
            }
        } catch (WMClientException $e) {

            $logger->error($e->getMessage(), ['code' => $e->getCode()]);

            $this->addFlash('error', 'Something went wrong! Please try again later.');
            return $this->redirectToRoute('app_registration_step', ['stepNumber' => $stepNumber]);
        }

        return $this->render('registration/step.html.twig', [
            'registration' => $registration
        ]);
    }


}