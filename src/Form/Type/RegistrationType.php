<?php

namespace App\Form\Type;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationType
 * @package App\Form\Type
 */
class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $stepNumber = isset($options['step_number']) ? abs((int)$options['step_number']) : 1;
        $stepBuilderName = sprintf('buildStep%d', $stepNumber);

        if(!is_callable([$this, $stepBuilderName])) {
            throw new \Exception("Unsupported registration step: {$stepNumber}.");
        }

        $this->$stepBuilderName($builder);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'step_number' => null,
            'validation_groups' => null
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function buildStep1(FormBuilderInterface $builder)
    {
        $builder
            ->add('firstName', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('lastName', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('telephone', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ]);
    }

    public function buildStep2(FormBuilderInterface $builder)
    {
        $builder
            ->add('street', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('houseNumber', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('zipCode', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ]);
    }

    public function buildStep3(FormBuilderInterface $builder)
    {
        $builder
            ->add('accountOwner', TextType::class, [
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('iban', TextType::class, [
                'attr' => [
                    'maxlength' => 34
                ]
            ]);
    }
}