<?php

namespace App\Form\MultiStep;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MultiStepForm
 * @package App\Form\MultiStep
 */
abstract class MultiStepForm
{
    protected $steps = [];
    protected $numberOfSteps = 0;
    protected $passedSteps = [];

    protected $stepNumber = null;
    protected $form = null;
    protected $entity = null;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var SessionInterface
     */
    protected $session;

    abstract protected function stateKey(): string;
    abstract protected function formClass(): string;
    abstract protected function entityClass(): string;
    abstract protected function configureSteps(): void;

    public function __construct(FormFactoryInterface $formFactory, RouterInterface $router, SessionInterface $session)
    {
        $this->router = $router;
        $this->formFactory = $formFactory;
        $this->session = $session;

        $this->configureSteps();
        $this->numberOfSteps = count($this->steps);

        $this->loadState();
    }

    public function getStepConfiguration(string $name)
    {
        if(!isset($this->steps[$this->stepNumber][$name])) {
            throw new \Exception('Invalid step configuration.');
        }

        return $this->steps[$this->stepNumber][$name];
    }

    public function getTitle(): string
    {
        return $this->getStepConfiguration('title');
    }

    public function getSubmitLabel(): string
    {
        $stepConfiguration = $this->getStepConfiguration('next');
        return isset($stepConfiguration['label']) ? $stepConfiguration['label'] : 'Submit';
    }

    public function getNextStepUrl(): string
    {
        $stepConfiguration = $this->getStepConfiguration('next');

        return $this->router->generate(
            $stepConfiguration['routeName'],
            ['stepNumber' => $this->stepNumber + 1]
        );
    }

    public function getPreviousButtonConfiguration(): array
    {
        $data = $this->getStepConfiguration('previous');
        $data['url'] = $this->router->generate($data['routeName'], [
            'stepNumber' => $this->getStepNumber() - 1
        ]);
        return $data;
    }

    public function hasPreviousStep(): bool
    {
        return $this->stepNumber > 1;
    }

    public function hasNextStep(): bool
    {
        return $this->stepNumber < $this->numberOfSteps;
    }

    /**
     * @param int $stepNumber
     * @return MultiStepForm
     * @throws \Exception
     */
    public function setStepNumber(int $stepNumber): self
    {
        if($stepNumber < 1 || $stepNumber > $this->numberOfSteps) {
            throw new \InvalidArgumentException("Incorrect step number: {$stepNumber}");
        }

        $this->stepNumber = $stepNumber;

        return $this;
    }

    public function isStepAllowed(int $stepNumber)
    {
        if($stepNumber == 1) {
            return true;
        }

        if(empty($this->passedSteps)) {
            return $stepNumber == 1;
        }

        return in_array($stepNumber - 1, $this->passedSteps);
    }


    public function getStepNumber(): int
    {
        return $this->stepNumber;
    }

    /**
     * @return bool
     */
    public function isLastStep(): bool
    {
        return $this->numberOfSteps === $this->stepNumber;
    }

    public function getLastPassedStep()
    {
        return !empty($this->passedSteps) ? max($this->passedSteps) : 1;
    }


    public function getForm(): Form
    {
        if($this->form === null) {
            $this->form = $this->formFactory->create($this->formClass(), $this->entity, [
                'step_number' => $this->stepNumber,
                'validation_groups' => ["step_{$this->stepNumber}"]
            ]);
        }

        return $this->form;
    }

    public function saveState(): void
    {
        $this->passedSteps[] = $this->getStepNumber();

        $this->session->set($this->stateKey(), [
            'entity' => $this->entity,
            'passed_steps' => $this->passedSteps
        ]);
    }

    public function loadState(): void
    {
        $state = $this->session->get($this->stateKey());
        if(!empty($state['entity'])) {
            $this->entity = $state['entity'];
        } else {
            $entityClass = $this->entityClass();
            $this->entity = new $entityClass();
        }

        $this->passedSteps = !empty($state['passed_steps']) ? $state['passed_steps'] : [];
    }

    public function removeInterrupted()
    {
        setcookie($this->getInterruptedCookieKey(), "", time() - 36000);
    }

    public function isInterrupted()
    {
        return isset($_COOKIE[$this->getInterruptedCookieKey()]) && $_COOKIE[$this->getInterruptedCookieKey()];
    }

    public function getInterruptedStepNumber()
    {
        return isset($_COOKIE[$this->getInterruptedCookieKey()]) ? abs((int)$_COOKIE[$this->getInterruptedCookieKey()]) : null;
    }

    public function getInterruptedCookieKey()
    {
        return $this->stateKey().'_interrupted';
    }


    protected function beforeFinish()
    {

    }

    public function isFinished()
    {
        $data = $this->session->get($this->stateKey());
        return !empty($data['finished']);
    }

    public function finish()
    {
        $this->beforeFinish();
        $this->session->set($this->stateKey(), ['finished' => true]);
    }
}