# README #

The application built at the top of Symfony 5 framework, MVC pattern. 
I prefer the MVC pattern for rapid development.

### Describe possible performance optimizations for your Code. ###
 
 * double insert on form finish
    * first for saving data,
    * second for saving payment data-id.
  *  maybe it is possible to finish registration with one insert,
  * session store in Redis,
  * many more optimizations for sure.
 
 
### Which things could be done better, than you’ve done it? ###
 
 * better design of the component of a multi-step form,
 * better error handling,
 * securing the application for uncommon uses of a form,
 * better handling of user re-navigation to the website.


 